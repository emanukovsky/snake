package net.kimleo.snake.model;

import java.util.Set;
import java.util.concurrent.ThreadLocalRandom;

public class FoodProducer {

	private int xMax;
	private int yMax;
	private Point foodPoint;
	
	public FoodProducer(Set<Point> snakePoints, int xMax, int yMax) {
		this.xMax = xMax;
		this.yMax = yMax;
		generateFoodPoint(snakePoints);
	}
	
	public void generateFoodPoint(Set<Point> snakePoints) {
		Point randomPoint = getRandomPoint();
		while (snakePoints.contains(randomPoint)) {
			randomPoint = getRandomPoint();
		}
		this.foodPoint = randomPoint;
	}

	public Point getFoodPoint() {
		return foodPoint;
	}

	private Point getRandomPoint() {
		int xRandom = ThreadLocalRandom.current().nextInt(0, xMax);
		int yRandom = ThreadLocalRandom.current().nextInt(0, yMax);
		Point point = new Point(xRandom, yRandom);
		return point;
	}
	
}
