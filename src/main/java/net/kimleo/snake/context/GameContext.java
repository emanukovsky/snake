package net.kimleo.snake.context;

import javafx.application.Platform;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import net.kimleo.snake.model.Point;
import net.kimleo.snake.model.Snake;
import net.kimleo.snake.model.SnakeNode;

public class GameContext {

	private GraphicsContext context;
	private final int weight;
	private final int height;
	private final int size;

	public GameContext(int weight, int height, int size, GraphicsContext context) {
		this.weight = weight * size;
		this.height = height * size;
		this.size = size;
		this.context = context;
		this.context.setFill(Color.BLUE);
		this.context.setStroke(Color.WHITE);
		this.context.setLineWidth(2);
	}

	public void draw(SnakeNode node) {
				Point position = node.position();
				int offSetX = size * position.x;
				int offSetY = size * position.y;
				//System.out.println("GameContext.draw(node), node: " + node);
				if (offSetX < weight 
						&& offSetY < height
						&& offSetX >= 0 
						&& offSetY >= 0) {
					drawRect(offSetX, offSetY);
				} else {
					System.out.println("Out of bound! position: " + position + 
							"; offSetX: " + offSetX + "; offSetY: " + offSetY);
					throw new RuntimeException("The snake is out of bounds.");
				}
	}
	
	private void drawRect(int offSetX, int offSetY) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				context.fillRect(offSetX, offSetY, size, size);
				context.strokeRect(offSetX, offSetY, size, size);
			}
		});
	}

	public void draw(Snake snake) {
		clearContext();
		SnakeNode node = snake.head();
		//System.out.println("\nhead: ");
		while (!node.isEmptyNode()) {
			draw(node);
			node = node.next();
		}
	}

	private void clearContext() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				context.clearRect(0, 0, weight, height);
			}
		});
	}

	public void draw(Point foodPoint) {
		//System.out.println("foodPoint = " + foodPoint);
		drawRect(foodPoint.x*size, foodPoint.y*size);
	}

}

