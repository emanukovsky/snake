package net.kimleo.snake;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.input.KeyCode;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import net.kimleo.snake.context.GameContext;
import net.kimleo.snake.model.Direction;
import net.kimleo.snake.model.FoodProducer;
import net.kimleo.snake.model.Snake;

import java.util.HashMap;

public class SnakeApplication extends Application {

	private static boolean isRunning = true;
	private static boolean isPause = false;
	private static long delay = 1000L;
	private static float delayChangeFactor = 0.75f;

	public static void main(String[] args) {
		launch(args);
	}

	private void closeWindowEvent(WindowEvent event) {
		//System.out.println("Window close request...");
		isRunning = false;
	}

	@Override
	public void start(Stage primaryStage) {
		int xMax = 30;
		int yMax = 30;
		int elementSize = 10;
		Canvas canvas = new Canvas(xMax*elementSize, yMax*elementSize);
		GraphicsContext graphicsContext2D = canvas.getGraphicsContext2D();
		GameContext gameContext = new GameContext(xMax, yMax, elementSize, graphicsContext2D);
		Group root = new Group();
		root.getChildren().add(canvas);
		HashMap<KeyCode, Direction> actions = new HashMap<KeyCode, Direction>() {{
			put(KeyCode.UP, Direction.UP);
			put(KeyCode.DOWN, Direction.DOWN);
			put(KeyCode.LEFT, Direction.LEFT);
			put(KeyCode.RIGHT, Direction.RIGHT);
		}};
		Scene scene = new Scene(root);
		Snake snake = new Snake();
		scene.setOnKeyPressed(event -> {
			KeyCode code = event.getCode();
			if (actions.containsKey(code)) {
				snake.heading(calculateDirection(snake.heading(), actions.get(code)));
			}
			if (KeyCode.PAUSE.equals(code)) {
				isPause = !isPause;
				System.out.println("isPause = " + isPause);
			}
			if (KeyCode.ADD.equals(code)) {
				increaseSpeed();
			}
			if (KeyCode.SUBTRACT.equals(code)) {
				decreaseSpeed();
			}
		});
		primaryStage.setTitle("Snake");
		primaryStage.setScene(scene);
		scene.getWindow().addEventFilter(WindowEvent.WINDOW_CLOSE_REQUEST, this::closeWindowEvent);
		primaryStage.show();
		FoodProducer foodProducer = new FoodProducer(snake.ownedPoints(), xMax, yMax);

		Thread thread = new Thread(() -> {
			while (isRunning) {
				if (!isPause) {
				try {
					snake.move(foodProducer);
					gameContext.draw(snake);
					gameContext.draw(foodProducer.getFoodPoint());
				} catch (Exception e) {
					isRunning = false;
					stopAlert(e.getMessage(), snake.size());
					}
				}
				try {
					Thread.sleep(delay);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		thread.start();
	}

	private void stopAlert(String message, int snakeSize) {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				Alert alert = new Alert(AlertType.INFORMATION);
				alert.setTitle("Game is over!");
				alert.setHeaderText(message +  "\nSnake size: " + snakeSize);
				alert.showAndWait();
				Platform.exit();
			}
		});
	}

	private static void increaseSpeed() {
		delay = (long) (delay * delayChangeFactor);
		System.out.println("increase delay, now it is: " + delay);
	}

	private static void decreaseSpeed() {
		delay = (long) (delay / delayChangeFactor);
		System.out.println("decrease delay, now it is: " + delay);
	}

	private static Direction calculateDirection(Direction currentDirection, Direction supposedDirection) {
		if (Direction.RIGHT.equals(currentDirection) && Direction.LEFT.equals(supposedDirection) ) {
			return Direction.RIGHT;
		}
		if (Direction.LEFT.equals(currentDirection) && Direction.RIGHT.equals(supposedDirection) ) {
			return Direction.LEFT;
		}
		if (Direction.UP.equals(currentDirection) && Direction.DOWN.equals(supposedDirection) ) {
			return Direction.UP;
		}
		if (Direction.DOWN.equals(currentDirection) && Direction.UP.equals(supposedDirection) ) {
			return Direction.DOWN;
		}
		return supposedDirection;
	}

}
